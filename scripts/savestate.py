# fast script to ease upload of auth_user on full db restore
# upserts users
#
# author: Asier Murciego Alonso


import psycopg2
import csv
import sys
import json


def main():
	op = None
	
	if len(sys.argv) < 4:
		print("Syntax: update_user.py dump|load {db_config_file} {suffix}")
	else:
		op = sys.argv[1]
		conn_params = json.load(open(sys.argv[2]))
		if op == "dump":
			dump(conn_params, sys.argv[3])
		elif op == "load":
			load(conn_params, sys.argv[3])
		else:
			print("Invalid op")
			print("Syntax: update_user.py dump|load {db_config_file} {auth_user_csvfile}")
			exit(-1)
	
	exit(0)
	

def load(conn_params, suffix):

	conn = psycopg2.connect(**conn_params)

	# auth_user must be upserted to prevent FK errors
	UPSERT_STMT = "INSERT INTO \
		auth_user (id, username, password, email, salt, active) \
	VALUES ({id}, '{username}', '{password}', '{email}', '{salt}', '{active}') \
	ON CONFLICT ON CONSTRAINT auth_user_pkey \
	DO \
		UPDATE SET username = EXCLUDED.username, password = EXCLUDED.password, email = EXCLUDED.email, salt = EXCLUDED.salt, active = EXCLUDED.active;"

	filename = "{}-{}.csv".format("auth_user", suffix)
	with open(filename) as csvfile:
		reader = csv.DictReader(csvfile, delimiter=',')
		for row in reader:
			with conn.cursor() as cur:
				cur.execute(UPSERT_STMT.format(**row))
				conn.commit()
				print("Upsert user {} ok,".format(row["id"]))
				
	# other tables with no FK problems 
	tables = ["auth_user_role"]
	for table in tables:
		filename = "{}-{}.csv".format(table, suffix)
		stmt = "TRUNCATE TABLE {0}; COPY {0} FROM STDIN WITH CSV HEADER".format(table)
		with open(filename, "r") as csvin:
			with conn.cursor() as cur:
				cur.copy_expert(stmt, csvin)
		print("'{}' table loaded to {}".format(table, filename))

	conn.close()

def dump(conn_params, suffix):
	"""
	Dumps database state: some tables that should be restored when recreating the database
	"""
	tables = ["auth_user", "auth_user_role"]
	
	for table in tables:
		filename = "{}-{}.csv".format(table, suffix)
		stmt = "COPY {} TO STDOUT WITH CSV HEADER".format(table)
		with open(filename, "wt") as csvout:
			with psycopg2.connect(**conn_params) as conn:
				with conn.cursor() as cur:
					cur.copy_expert(stmt, csvout)
		print("'{}' table dumped to {}".format(table, filename))


if __name__ == "__main__":
	main()